# makefiles

## usage

#### docker
```
cd /path/to/docker/dir/
ln -s /path/to/this/repo/docker Makefile
make build
make deploy
```

Check the file for all possible make options.

#### terraform
```
cd /path/to/terraform/dir/
ln -s /path/to/this/repo/terraform Makefile
make plan
make apply
```

Check the file for all possible make options.

## License

See LICENSE file
